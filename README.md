# Tree

Hi Everyone this was a little servlet created for technical test to apply in Masivian

## Getting Started

Just package the project with maven and then use java -jar to run target/tree-1.0.jar

### Prerequisites

You need java and maven

```
mvn package
```

## Running the tests

```
mvn test
```

## Examples

In the repository you can find a postman collection that will allow to use you the main endpoints

