package org.acastane.tree.utils;

import org.acastane.tree.core.Left;
import org.mockito.ArgumentMatcher;

/**
 * Created by angelica on 10/02/19.
 */
public class LeftMatcher implements ArgumentMatcher<Left> {

    private Left left;

    public LeftMatcher(Left left) {
        this.left = left;
    }

    @Override
    public boolean matches(Left right) {
        return left.getTree().equals(right.getTree()) &&
                left.getParent().equals(right.getParent()) &&
                left.getNumber().equals(right.getNumber());
    }
}
