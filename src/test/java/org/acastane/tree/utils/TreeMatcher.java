package org.acastane.tree.utils;

import org.acastane.tree.core.Tree;
import org.mockito.ArgumentMatcher;

/**
 * Created by angelica on 10/02/19.
 */
public class TreeMatcher implements ArgumentMatcher<Tree> {

    private Tree left;

    public TreeMatcher(Tree left) {
        this.left = left;
    }

    @Override
    public boolean matches(Tree right) {
        return left.getId().equals(right.getId());
    }
}
