package org.acastane.tree.utils;

import org.acastane.tree.core.Right;
import org.mockito.ArgumentMatcher;

/**
 * Created by angelica on 10/02/19.
 */
public class RightMatcher implements ArgumentMatcher<Right> {

    private Right left;

    public RightMatcher(Right left) {
        this.left = left;
    }

    @Override
    public boolean matches(Right right) {
        return left.getTree().equals(right.getTree()) &&
                left.getParent().equals(right.getParent()) &&
                left.getNumber().equals(right.getNumber());
    }
}
