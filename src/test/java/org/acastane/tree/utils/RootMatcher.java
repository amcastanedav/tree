package org.acastane.tree.utils;

import org.acastane.tree.core.Root;
import org.mockito.ArgumentMatcher;

/**
 * Created by angelica on 10/02/19.
 */
public class RootMatcher implements ArgumentMatcher<Root> {

    private Root left;

    public RootMatcher(Root left) {
        this.left = left;
    }

    @Override
    public boolean matches(Root right) {
        return left.getTree().equals(right.getTree()) &&
                left.getNumber().equals(right.getNumber());
    }
}
