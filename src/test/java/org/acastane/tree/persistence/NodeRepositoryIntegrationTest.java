package org.acastane.tree.persistence;

import org.acastane.tree.core.Left;
import org.acastane.tree.core.Node;
import org.acastane.tree.core.Right;
import org.acastane.tree.core.Root;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

/**
 * Created by angelica on 9/02/19.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class NodeRepositoryIntegrationTest {

    @Autowired
    private NodeRepository nodeRepository;

    @Test
    public void whenRootDoesntExistRootThenRootIsNotFound() {
        Assert.assertFalse(nodeRepository.existRoot("a"));
        Optional<Node> result = nodeRepository.findRootByTree("a");
        Assert.assertFalse(nodeRepository.existRoot("a"));
        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void whenRootIsCreatedThenRootIsFound() {
        Root root = new Root("b", 5);

        nodeRepository.save(root.toNode());
        Optional<Node> result = nodeRepository.findRootByTree("b");

        Assert.assertTrue(nodeRepository.existRoot("b"));
        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(root.getTree(), result.get().getTree());
        Assert.assertEquals(root.getNumber(), result.get().getNumber());
        Assert.assertEquals(root.getParent(), result.get().getParent());
        Assert.assertFalse(result.get().getLeftc());
        Assert.assertFalse(result.get().getRightc());
    }

    @Test
    public void whenRootIsDeletedThenRootIsNotFound() {
        Root root = new Root("c", 6);

        nodeRepository.save(root.toNode());
        nodeRepository.deleteTree("c");

        Optional<Node> result = nodeRepository.findRootByTree("c");
        Assert.assertFalse(nodeRepository.existRoot("c"));
        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void whenNodeDoesntExistThenNodeIsNotFoundByTreeAndNumber() {
        Assert.assertFalse(nodeRepository.existNodeByTreeAndNumber("d", 4));
    }

    @Test
    public void whenNodeIsCreatedThenNodeIsFoundByTreeAndNumber() {
        Node node = new Node("e", 0, 3, false, false);

        nodeRepository.save(node);

        Assert.assertTrue(nodeRepository.existNodeByTreeAndNumber("e", 3));
    }

    @Test
    public void whenNodeIsDeletedThenIsNotFoundByTreeAndNumber() {
        Node node = new Node("f", 0, 6, false, false);

        nodeRepository.save(node);
        nodeRepository.deleteTree("f");

        Assert.assertFalse(nodeRepository.existNodeByTreeAndNumber("f", 6));
    }

    @Test
    public void whenNodeDoesntExistThenNodeIsNotFoundByTreeAndParent() {
        Optional<Node> result = nodeRepository.findNodeByTreeAndParent("g", 9, false, false);
        Assert.assertFalse(nodeRepository.existNodeByTreeAndParent("g", 9, false, false));
        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void whenNodeIsCreatedThenIsFoundByTreeAndParent() {
        Node node = new Node("h", 0, 8, true, false);

        nodeRepository.save(node);

        Optional<Node> result = nodeRepository.findNodeByTreeAndParent("h", 0, true, false);
        Assert.assertTrue(nodeRepository.existNodeByTreeAndParent("h", 0, true, false));
        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(node.getTree(), result.get().getTree());
        Assert.assertEquals(node.getParent(), result.get().getParent());
        Assert.assertEquals(node.getNumber(), result.get().getNumber());
        Assert.assertEquals(node.getLeftc(), result.get().getLeftc());
        Assert.assertEquals(node.getRightc(), result.get().getRightc());
    }

    @Test
    public void whenNodeIsDeletedThenIsNotFoundByTreeAndParent() {
        Node node = new Node("i", 0, 1, false, true);

        nodeRepository.save(node);
        nodeRepository.deleteTree("i");

        Optional<Node> result = nodeRepository.findNodeByTreeAndParent("i", 0, false, true);
        Assert.assertFalse(nodeRepository.existNodeByTreeAndParent("i", 0, false, true));
        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void whenNodeDoesntHaveKidsThenKidsAreNotFound() {
        Node node = new Node("j", 0, 9, false, false);

        nodeRepository.save(node);

        Assert.assertFalse(nodeRepository.hasKids("j", 9));
    }

    @Test
    public void whenNodeHasKidsThenKidsAreFound() {
        Node node = new Node("k", 0, 6, false, false);
        Node child = new Node("k", 6, 3, true, false);

        nodeRepository.save(node);
        nodeRepository.save(child);

        Assert.assertTrue(nodeRepository.hasKids("k", 6));
    }

    @Test
    public void whenNodeIsDeleteThenNodeIsNotFound() {
        Root root = new Root("l", 4);
        Left left = new Left("l", 4, 8);
        Right right = new Right("l", 4, 9);

        nodeRepository.save(root.toNode());
        nodeRepository.save(left.toNode());
        nodeRepository.save(right.toNode());
        nodeRepository.deleteNode("l", 8);

        Assert.assertFalse(nodeRepository.existNodeByTreeAndNumber("l", 8));
        Assert.assertFalse(nodeRepository.existNodeByTreeAndParent("l", 4, true, false));
        Assert.assertTrue(nodeRepository.existRoot("l"));
        Assert.assertTrue(nodeRepository.hasKids("l", 4));
        Assert.assertTrue(nodeRepository.existNodeByTreeAndParent("l", 4, false, true));

        Optional<Node> rootResult = nodeRepository.findRootByTree("l");
        Optional<Node> leftResult = nodeRepository.findNodeByTreeAndParent("l", 4, true, false);
        Optional<Node> rightResult = nodeRepository.findNodeByTreeAndParent("l", 4, false, true);

        Assert.assertTrue(rootResult.isPresent());
        Assert.assertFalse(leftResult.isPresent());
        Assert.assertTrue(rightResult.isPresent());
    }

    @Test
    public void whenTreeIsDeletedThenNoNodesAreFound() {
        Root root = new Root("m", 1);
        Left left = new Left("m", 1, 9);
        Right right = new Right("m", 1, 4);

        nodeRepository.save(root.toNode());
        nodeRepository.save(left.toNode());
        nodeRepository.save(right.toNode());
        nodeRepository.deleteTree("m");

        Assert.assertFalse(nodeRepository.existRoot("m"));
        Assert.assertFalse(nodeRepository.existNodeByTreeAndNumber("m", 9));
        Assert.assertFalse(nodeRepository.existNodeByTreeAndNumber("m", 4));
        Assert.assertFalse(nodeRepository.existNodeByTreeAndParent("m", 1, true, false));
        Assert.assertFalse(nodeRepository.existNodeByTreeAndParent("m", 1, false, true));

        Optional<Node> rootResult = nodeRepository.findRootByTree("m");
        Optional<Node> leftResult = nodeRepository.findNodeByTreeAndParent("m", 1, true, false);
        Optional<Node> rightResult = nodeRepository.findNodeByTreeAndParent("m", 1, false, true);

        Assert.assertFalse(rootResult.isPresent());
        Assert.assertFalse(leftResult.isPresent());
        Assert.assertFalse(rightResult.isPresent());
    }

    @Test
    public void whenTreeIsCreatedThenRootAndNodesAreFound() {
        Root root = new Root("n", 5);
        Left left = new Left("n", 5, 1);
        Right right = new Right("n", 5, 3);

        nodeRepository.save(root.toNode());
        nodeRepository.save(left.toNode());
        nodeRepository.save(right.toNode());

        Assert.assertTrue(nodeRepository.existRoot("n"));
        Assert.assertTrue(nodeRepository.existNodeByTreeAndNumber("n", 5));
        Assert.assertTrue(nodeRepository.existNodeByTreeAndNumber("n", 1));
        Assert.assertTrue(nodeRepository.existNodeByTreeAndNumber("n", 3));
        Assert.assertTrue(nodeRepository.existNodeByTreeAndParent("n", 5, true, false));
        Assert.assertTrue(nodeRepository.existNodeByTreeAndParent("n", 5, false, true));
        Assert.assertTrue(nodeRepository.hasKids("n", 5));

        Optional<Node> rootResult = nodeRepository.findRootByTree("n");
        Optional<Node> leftResult = nodeRepository.findNodeByTreeAndParent("n", 5, true, false);
        Optional<Node> rightResult = nodeRepository.findNodeByTreeAndParent("n", 5, false, true);

        Assert.assertTrue(rootResult.isPresent());
        Assert.assertTrue(leftResult.isPresent());
        Assert.assertTrue(rightResult.isPresent());

        Assert.assertEquals(left.getNumber(), leftResult.get().getNumber());
        Assert.assertEquals(right.getNumber(), rightResult.get().getNumber());
    }
}
