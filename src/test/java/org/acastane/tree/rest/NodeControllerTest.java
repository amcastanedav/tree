package org.acastane.tree.rest;

import org.acastane.tree.core.Left;
import org.acastane.tree.core.Right;
import org.acastane.tree.core.Root;
import org.acastane.tree.exceptions.HasKidsException;
import org.acastane.tree.exceptions.NodeAlreadyExistException;
import org.acastane.tree.exceptions.ParentDoesNotExistException;
import org.acastane.tree.exceptions.TreeDoesNotExistException;
import org.acastane.tree.service.TreeService;
import org.acastane.tree.utils.LeftMatcher;
import org.acastane.tree.utils.RightMatcher;
import org.acastane.tree.utils.RootMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by angelica on 10/02/19.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(NodeController.class)
public class NodeControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TreeService treeService;

    private String exceptionMessage = "New exception message";
    private String badRequestNode = "Your request must contain parent and number and must be integer";

    @Test
    public void whenRootExistThenRootCanBeRetrieved() throws Exception {
        Root root = new Root("a", 6);
        given(treeService.findRootByTree("a")).willReturn(Optional.of(root));

        mvc.perform(get("/node/root/a"))
                .andExpect(status().isOk())
                .andExpect(content().string(root.toString()));
    }

    @Test
    public void whenRootDoesntExistsThenRootCanNotBeRetrieved() throws Exception {
        given(treeService.findRootByTree("b")).willReturn(Optional.empty());

        mvc.perform(get("/node/root/b"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("wasn't not found")));
    }

    @Test
    public void whenBadRequestThenRootCanNotBeCreated() throws Exception {
        String badRequestRoot = "Number not found";

        mvc.perform(post("/node/root/c").contentType(MediaType.APPLICATION_JSON).content(""))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/node/root/c").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestRoot));

        mvc.perform(post("/node/root/c").contentType(MediaType.APPLICATION_JSON).content("{\"valor\":\"1\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestRoot));

        mvc.perform(post("/node/root/c").contentType(MediaType.APPLICATION_JSON).content("{\"number\":\"hola\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestRoot));
    }

    @Test
    public void whenTreeDoesntExistsThenRootCanNotBeCreated() throws Exception {
        doThrow(new TreeDoesNotExistException(exceptionMessage)).when(treeService).saveRoot(argThat(new RootMatcher(new Root("d", 5))));

        mvc.perform(post("/node/root/d").contentType(MediaType.APPLICATION_JSON).content("{\"number\":\"5\"}"))
                .andExpect(status().isConflict())
                .andExpect(content().string(exceptionMessage));
    }

    @Test
    public void whenRootAlreadyExistsThenRootCanNotBeCreated() throws Exception {
        doThrow(new NodeAlreadyExistException(exceptionMessage)).when(treeService).saveRoot(argThat(new RootMatcher(new Root("e", 7))));

        mvc.perform(post("/node/root/e").contentType(MediaType.APPLICATION_JSON).content("{\"number\":\"7\"}"))
                .andExpect(status().isConflict())
                .andExpect(content().string(exceptionMessage));
    }

    @Test
    public void whenNewRootThenRootCanBeCreated() throws Exception {
        mvc.perform(post("/node/root/f").contentType(MediaType.APPLICATION_JSON).content("{\"number\":\"1\"}"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("You created root")));
    }

    @Test
    public void whenBadRequestThenLeftCanNotBeRetrieved() throws Exception {
        mvc.perform(get("/node/left/la/hola"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("not an integer")));
    }

    @Test
    public void whenLeftDoestExistThenLeftCanNotBeRetrieved() throws Exception {
        given(treeService.findLeftByTreeAndParent("lb", 6)).willReturn(Optional.empty());

        mvc.perform(get("/node/left/lb/6"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("was not found")));
    }

    @Test
    public void whenLeftExistsThenLeftCanBeRetrieved() throws Exception {
        Left left = new Left("lc", 9, 8);
        given(treeService.findLeftByTreeAndParent("lc", 9)).willReturn(Optional.of(left));

        mvc.perform(get("/node/left/lc/9"))
                .andExpect(status().isOk())
                .andExpect(content().string(left.toString()));
    }

    @Test
    public void whenBadRequestThenLeftCanNotBeCreated() throws Exception {
        mvc.perform(post("/node/left/ld").contentType(MediaType.APPLICATION_JSON).content(""))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/node/left/ld").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestNode));

        mvc.perform(post("/node/left/ld").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"hola\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestNode));

        mvc.perform(post("/node/left/ld").contentType(MediaType.APPLICATION_JSON).content("{\"number\":\"hola\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestNode));

        mvc.perform(post("/node/left/ld").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"5\",\"number\":\"hola\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestNode));

        mvc.perform(post("/node/left/ld").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"hola\",\"number\":\"9\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestNode));
    }

    @Test
    public void whenTreeDoesntExistThenLeftCanNotBeCreated() throws Exception {
        doThrow(new TreeDoesNotExistException(exceptionMessage)).when(treeService).saveLeft(argThat(new LeftMatcher(new Left("le", 7, 9))));

        mvc.perform(post("/node/left/le").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"7\",\"number\":\"9\"}"))
                .andExpect(status().isConflict())
                .andExpect(content().string(exceptionMessage));
    }

    @Test
    public void whenParentDoesntExistsThenLeftCanNotBeCreated() throws Exception {
        doThrow(new ParentDoesNotExistException(exceptionMessage)).when(treeService).saveLeft(argThat(new LeftMatcher(new Left("lf", 7, 9))));

        mvc.perform(post("/node/left/lf").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"7\",\"number\":\"9\"}"))
                .andExpect(status().isConflict())
                .andExpect(content().string(exceptionMessage));
    }

    @Test
    public void whenLeftAlreadyExistsThenLeftCanNotBeCreated() throws Exception {
        doThrow(new NodeAlreadyExistException(exceptionMessage)).when(treeService).saveLeft(argThat(new LeftMatcher(new Left("lg", 7, 9))));

        mvc.perform(post("/node/left/lg").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"7\",\"number\":\"9\"}"))
                .andExpect(status().isConflict())
                .andExpect(content().string(exceptionMessage));
    }

    @Test
    public void whenNewLeftThenLeftIsCreated() throws Exception {
        mvc.perform(post("/node/left/lh").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"7\",\"number\":\"9\"}"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("You created left")));
    }

    @Test
    public void whenBadRequestThenRightCanNotBeRetrieved() throws Exception {
        mvc.perform(get("/node/right/ra/hola"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("not an integer")));
    }

    @Test
    public void whenRightDoestExistThenRightCanNotBeRetrieved() throws Exception {
        given(treeService.findRightByTreeAndParent("rb", 6)).willReturn(Optional.empty());

        mvc.perform(get("/node/right/rb/6"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("was not found")));
    }

    @Test
    public void whenRightExistsThenRightCanBeRetrieved() throws Exception {
        Right right = new Right("rc", 9, 8);
        given(treeService.findRightByTreeAndParent("rc", 9)).willReturn(Optional.of(right));

        mvc.perform(get("/node/right/rc/9"))
                .andExpect(status().isOk())
                .andExpect(content().string(right.toString()));
    }

    @Test
    public void whenBadRequestThenRightCanNotBeCreated() throws Exception {
        mvc.perform(post("/node/right/rd").contentType(MediaType.APPLICATION_JSON).content(""))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/node/right/rd").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestNode));

        mvc.perform(post("/node/right/rd").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"hola\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestNode));

        mvc.perform(post("/node/right/rd").contentType(MediaType.APPLICATION_JSON).content("{\"number\":\"hola\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestNode));

        mvc.perform(post("/node/right/rd").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"5\",\"number\":\"hola\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestNode));

        mvc.perform(post("/node/right/rd").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"hola\",\"number\":\"9\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestNode));
    }

    @Test
    public void whenTreeDoesntExistThenRightCanNotBeCreated() throws Exception {
        doThrow(new TreeDoesNotExistException(exceptionMessage)).when(treeService).saveRight(argThat(new RightMatcher(new Right("re", 7, 9))));

        mvc.perform(post("/node/right/re").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"7\",\"number\":\"9\"}"))
                .andExpect(status().isConflict())
                .andExpect(content().string(exceptionMessage));
    }

    @Test
    public void whenParentDoesntExistsThenRightCanNotBeCreated() throws Exception {
        doThrow(new ParentDoesNotExistException(exceptionMessage)).when(treeService).saveRight(argThat(new RightMatcher(new Right("rf", 7, 9))));

        mvc.perform(post("/node/right/rf").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"7\",\"number\":\"9\"}"))
                .andExpect(status().isConflict())
                .andExpect(content().string(exceptionMessage));
    }

    @Test
    public void whenRightAlreadyExistsThenRightCanNotBeCreated() throws Exception {
        doThrow(new NodeAlreadyExistException(exceptionMessage)).when(treeService).saveRight(argThat(new RightMatcher(new Right("rg", 7, 9))));

        mvc.perform(post("/node/right/rg").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"7\",\"number\":\"9\"}"))
                .andExpect(status().isConflict())
                .andExpect(content().string(exceptionMessage));
    }

    @Test
    public void whenNewLeftThenRightIsCreated() throws Exception {
        mvc.perform(post("/node/right/rh").contentType(MediaType.APPLICATION_JSON).content("{\"parent\":\"7\",\"number\":\"9\"}"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("You created right")));
    }

    @Test
    public void whenBadRequestThenAncestorCanNotBeFound() throws Exception {
        String badReuestAncestor = "Your request must contain number1 and number2 and must be integer";

        mvc.perform(post("/node/ancestor/aa").contentType(MediaType.APPLICATION_JSON).content(""))
                .andExpect(status().isBadRequest());

        mvc.perform(post("/node/ancestor/aa").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badReuestAncestor));

        mvc.perform(post("/node/ancestor/aa").contentType(MediaType.APPLICATION_JSON).content("{\"number1\":\"1\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badReuestAncestor));

        mvc.perform(post("/node/ancestor/aa").contentType(MediaType.APPLICATION_JSON).content("{\"number2\":\"2\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badReuestAncestor));

        mvc.perform(post("/node/ancestor/aa").contentType(MediaType.APPLICATION_JSON).content("{\"number1\":\"1\", \"number2\":\"hola\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badReuestAncestor));

        mvc.perform(post("/node/ancestor/aa").contentType(MediaType.APPLICATION_JSON).content("{\"number1\":\"1\", \"number2\":\"hola\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badReuestAncestor));
    }

    @Test
    public void whenTreeDoesntExistThenAncestorCanNotBeFound() throws Exception {
        given(treeService.findAncestor("ab",1,2)).willThrow(new TreeDoesNotExistException(exceptionMessage));

        mvc.perform(post("/node/ancestor/ab").contentType(MediaType.APPLICATION_JSON).content("{\"number1\":\"1\", \"number2\":\"2\"}"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(exceptionMessage));
    }

    @Test
    public void whenNoAncestorIsFoundThenNoAncestorIsRetrieved() throws Exception {
        given(treeService.findAncestor("ac", 1, 2)).willReturn(Optional.empty());

        mvc.perform(post("/node/ancestor/ac").contentType(MediaType.APPLICATION_JSON).content("{\"number1\":\"1\", \"number2\":\"2\"}"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("No common ancestor found"));
    }

    @Test
    public void whenAncestorIsFoundThenAncestorIsRetrieved() throws Exception {
        given(treeService.findAncestor("ad", 1, 2)).willReturn(Optional.of(5));

        mvc.perform(post("/node/ancestor/ad").contentType(MediaType.APPLICATION_JSON).content("{\"number1\":\"1\", \"number2\":\"2\"}"))
                .andExpect(status().isOk())
                .andExpect(content().string("5"));
    }

    @Test
    public void whenBadRequestThenNodeCanNotBeDeleted() throws Exception {
        String badRequestDelete = "Your request must contain number and must be integer";

        mvc.perform(delete("/node/da").contentType(MediaType.APPLICATION_JSON).content(""))
                .andExpect(status().isBadRequest());

        mvc.perform(delete("/node/da").contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestDelete));

        mvc.perform(delete("/node/da").contentType(MediaType.APPLICATION_JSON).content("{\"valor\":\"5\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestDelete));

        mvc.perform(delete("/node/da").contentType(MediaType.APPLICATION_JSON).content("{\"number\":\"hola\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(badRequestDelete));
    }

    @Test
    public void whenTreeDoesntExistThenNodeCanNotBeDeleted() throws Exception {
        doThrow(new TreeDoesNotExistException(exceptionMessage)).when(treeService).deleteNode("db", 5);

        mvc.perform(delete("/node/db").contentType(MediaType.APPLICATION_JSON).content("{\"number\":\"5\"}"))
                .andExpect(status().isConflict())
                .andExpect(content().string(exceptionMessage));
    }

    @Test
    public void whenNodeHasKidsThenNodeCanNotBeDeleted() throws Exception {
        doThrow(new HasKidsException(exceptionMessage)).when(treeService).deleteNode("dc", 5);

        mvc.perform(delete("/node/dc").contentType(MediaType.APPLICATION_JSON).content("{\"number\":\"5\"}"))
                .andExpect(status().isConflict())
                .andExpect(content().string(exceptionMessage));
    }

    @Test
    public void whenNodeThenNodeCanBeDeleted() throws Exception {
        mvc.perform(delete("/node/de").contentType(MediaType.APPLICATION_JSON).content("{\"number\":\"5\"}"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("deleted")));
    }
}
