package org.acastane.tree.rest;

import org.acastane.tree.core.Tree;
import org.acastane.tree.exceptions.TreeAlreadyExistException;
import org.acastane.tree.exceptions.TreeDoesNotExistException;
import org.acastane.tree.service.TreeService;
import org.acastane.tree.utils.TreeMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by angelica on 10/02/19.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(TreeController.class)
public class TreeControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TreeService treeService;

    @Test
    public void whenTreeExistThenTreeCanBeRetrieved() throws Exception {
        Tree tree = new Tree("a");
        given(treeService.findTreeById("a")).willReturn(Optional.of(tree));

        mvc.perform(get("/tree/a"))
                .andExpect(status().isOk())
                .andExpect(content().string(tree.toString()));
    }

    @Test
    public void whenTreeDoesntExistThenTreeCanNotBeRetrieved() throws Exception {
        given(treeService.findTreeById("b")).willReturn(Optional.empty());

        mvc.perform(get("/tree/b"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("Tree not found")));
    }

    @Test
    public void whenTreeDoesntExistsThenTreeCanBeCreated() throws Exception {
        mvc.perform(post("/tree/c"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("You created tree")));
    }

    @Test
    public void whenTreeAlreadyExistsThenTreeCanNotBeCreated() throws Exception {
        doThrow(new TreeAlreadyExistException("Tree already exists")).when(treeService).saveTree(argThat(new TreeMatcher(new Tree("d"))));

        mvc.perform(post("/tree/d"))
                .andExpect(status().isConflict())
                .andExpect(content().string("Tree already exists"));
    }

    @Test
    public void whenTreeDoesntExistsThenTreeCanNotBeDeleted() throws Exception {
        doThrow(new TreeDoesNotExistException("Tree doesn't exist")).when(treeService).deleteTree(argThat(new TreeMatcher(new Tree("e"))));

        mvc.perform(delete("/tree/e"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Tree doesn't exist"));

    }

    @Test
    public void whenTreeExistThenTreeCanBeDeleted() throws Exception {
        mvc.perform(delete("/tree/f"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("You deleted tree")));
    }
}
