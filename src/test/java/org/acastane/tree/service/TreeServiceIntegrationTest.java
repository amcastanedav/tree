package org.acastane.tree.service;

import org.acastane.tree.core.*;
import org.acastane.tree.exceptions.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by angelica on 9/02/19.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TreeServiceIntegrationTest {

    @Autowired
    private TreeService treeService;

    @Test
    public void whenTreeIsCreatedThenTreeIsFound() throws TreeAlreadyExistException {
        Tree tree = new Tree("a");

        treeService.saveTree(tree);
        Optional<Tree> result = treeService.findTreeById("a");

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(tree.getId(), result.get().getId());
    }

    @Test(expected = TreeAlreadyExistException.class)
    public void whenTreeAlreadyExistThenTreeCanNotBeCreatedAgain() throws TreeAlreadyExistException {
        Tree tree = new Tree("b");

        treeService.saveTree(tree);
        treeService.saveTree(tree);
    }

    @Test
    public void whenTreeIsDeletedThenTreeCanNotBeFound() throws TreeAlreadyExistException, TreeDoesNotExistException {
        Tree tree = new Tree("c");

        treeService.saveTree(tree);
        treeService.deleteTree(tree);
        Optional<Tree> result = treeService.findTreeById("c");

        Assert.assertFalse(result.isPresent());
    }

    @Test(expected = TreeDoesNotExistException.class)
    public void whenTreeDoesntExistThenTreeCanNotBeDeleted() throws TreeDoesNotExistException {
        Tree tree = new Tree("d");

        treeService.deleteTree(tree);
    }

    @Test(expected = TreeDoesNotExistException.class)
    public void whenTreeDoesntExistThenRootCanNotBeCreated() throws TreeDoesNotExistException, NodeAlreadyExistException {
        Root root = new Root("e", 5);

        treeService.saveRoot(root);
    }

    @Test
    public void whenRootIsSavedThenRootCanBeFound() throws TreeAlreadyExistException, TreeDoesNotExistException, NodeAlreadyExistException {
        Tree tree = new Tree("f");
        Root root = new Root("f", 8);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        Optional<Root> result = treeService.findRootByTree("f");

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(root.getTree(), result.get().getTree());
        Assert.assertEquals(root.getNumber(), result.get().getNumber());
    }

    @Test(expected = NodeAlreadyExistException.class)
    public void whenRootIsAlreadyCreatedTheRootCanNotBeCreatedAgain() throws TreeAlreadyExistException, TreeDoesNotExistException, NodeAlreadyExistException {
        Tree tree = new Tree("g");
        Root root = new Root("g", 1);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        treeService.saveRoot(root);
    }

    @Test
    public void whenRootIsDeletedThenRootCanBeFound() throws TreeAlreadyExistException, TreeDoesNotExistException, NodeAlreadyExistException, HasKidsException {
        Tree tree = new Tree("i");
        Root root = new Root("i", 4);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        treeService.deleteNode("i", 4);
        Optional<Root> result = treeService.findRootByTree("i");

        Assert.assertFalse(result.isPresent());
    }

    @Test(expected = HasKidsException.class)
    public void whenRootHasKidsThenRootCanNotBeDeleted() throws TreeAlreadyExistException, TreeDoesNotExistException, NodeAlreadyExistException, ParentDoesNotExistException, HasKidsException {
        Tree tree = new Tree("h");
        Root root = new Root("h", 9);
        Left left = new Left("h", 9, 6);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        treeService.saveLeft(left);
        treeService.deleteNode("h", 9);
    }

    @Test(expected = TreeDoesNotExistException.class)
    public void whenTreeDoesntExistThenLeftCanNotBeCreated() throws NodeAlreadyExistException, ParentDoesNotExistException, TreeDoesNotExistException {
        Left left = new Left("la", 0, 6);

        treeService.saveLeft(left);
    }

    @Test(expected = ParentDoesNotExistException.class)
    public void whenParentDoesntExistThenLeftCanNotBeCreated() throws TreeAlreadyExistException, NodeAlreadyExistException, ParentDoesNotExistException, TreeDoesNotExistException {
        Tree tree = new Tree("lb");
        Left left = new Left("lb", 0, 9);

        treeService.saveTree(tree);
        treeService.saveLeft(left);
    }

    @Test(expected = NodeAlreadyExistException.class)
    public void whenLeftAlreadyExistThenLeftCanNotBeCreatedAgain() throws TreeAlreadyExistException, TreeDoesNotExistException, NodeAlreadyExistException, ParentDoesNotExistException {
        Tree tree = new Tree("lc");
        Root root = new Root("lc", 9);
        Left left = new Left("lc", 9, 4);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        treeService.saveLeft(left);
        treeService.saveLeft(left);
    }

    @Test
    public void whenLeftIsDeletedThenLeftCanNotBeFound() throws TreeAlreadyExistException, TreeDoesNotExistException, NodeAlreadyExistException, ParentDoesNotExistException, HasKidsException {
        Tree tree = new Tree("ld");
        Root root = new Root("ld", 5);
        Left left = new Left("ld", 5, 1);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        treeService.saveLeft(left);
        treeService.deleteNode("ld", 1);
        Optional<Left> result = treeService.findLeftByTreeAndParent("ld", 5);

        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void whenLeftIsCreatedThenLeftCanBeFound() throws TreeAlreadyExistException, TreeDoesNotExistException, NodeAlreadyExistException, ParentDoesNotExistException {
        Tree tree = new Tree("le");
        Root root = new Root("le", 7);
        Left left = new Left("le", 7, 1);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        treeService.saveLeft(left);
        Optional<Left> result = treeService.findLeftByTreeAndParent("le", 7);

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(left.getTree(), result.get().getTree());
        Assert.assertEquals(left.getParent(), result.get().getParent());
        Assert.assertEquals(left.getNumber(), result.get().getNumber());
    }

    @Test(expected = TreeDoesNotExistException.class)
    public void whenTreeDoesntExistThenRightCanNotBeCreated() throws NodeAlreadyExistException, ParentDoesNotExistException, TreeDoesNotExistException {
        Right right = new Right("ra", 1, 5);

        treeService.saveRight(right);
    }

    @Test(expected = ParentDoesNotExistException.class)
    public void whenParentDoesntExistThenRightCanNotBeCreated() throws TreeAlreadyExistException, NodeAlreadyExistException, ParentDoesNotExistException, TreeDoesNotExistException {
        Tree tree = new Tree("rb");
        Right right = new Right("rb", 9, 7);

        treeService.saveTree(tree);
        treeService.saveRight(right);
    }

    @Test(expected = NodeAlreadyExistException.class)
    public void whenRightAlredyexistThenRightCanNotBeCreatedAgain() throws TreeAlreadyExistException, NodeAlreadyExistException, ParentDoesNotExistException, TreeDoesNotExistException {
        Tree tree = new Tree("rc");
        Root root = new Root("rc", 4);
        Right right = new Right("rc", 4, 5);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        treeService.saveRight(right);
        treeService.saveRight(right);
    }

    @Test
    public void whenRightIsDeletedThenRightCanNotBeFound() throws TreeAlreadyExistException, TreeDoesNotExistException, NodeAlreadyExistException, ParentDoesNotExistException, HasKidsException {
        Tree tree = new Tree("rd");
        Root root = new Root("rd", 7);
        Right right = new Right("rd", 7, 5);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        treeService.saveRight(right);
        treeService.deleteNode("rd", 5);
        Optional<Right> result = treeService.findRightByTreeAndParent("rd", 7);

        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void whenRightIsCreatedThenRightCanBeFound() throws TreeAlreadyExistException, TreeDoesNotExistException, NodeAlreadyExistException, ParentDoesNotExistException {
        Tree tree = new Tree("re");
        Root root = new Root("re", 1);
        Right right = new Right("re", 1, 7);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        treeService.saveRight(right);
        Optional<Right> result = treeService.findRightByTreeAndParent("re", 1);

        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(right.getTree(), result.get().getTree());
        Assert.assertEquals(right.getParent(), result.get().getParent());
        Assert.assertEquals(right.getNumber(), result.get().getNumber());
    }

    @Test(expected = TreeDoesNotExistException.class)
    public void whenTreeDoesntExistThenNoAncestorCanBeFound() throws TreeDoesNotExistException {
        treeService.findAncestor("aa", 1, 2);
    }

    @Test
    public void whenNodeDoesntExistThenNoAncestorCanBeFound() throws Exception {
        Tree tree = new Tree("ab");
        Root root = new Root("ab", 8);
        Left left = new Left("ab", 8, 4);
        Right right = new Right("ab", 8, 3);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        treeService.saveLeft(left);
        treeService.saveRight(right);

        Optional<Integer> result1 = treeService.findAncestor("ab", 4, 7);
        Optional<Integer> result2 = treeService.findAncestor("ab", 6, 3);

        Assert.assertFalse(result1.isPresent());
        Assert.assertFalse(result2.isPresent());
    }

    @Test
    public void whenTreeThenAncestorsCanBeFound() throws Exception {
        String treeId = "ac";
        Tree tree = new Tree(treeId);
        Root root = new Root(treeId, 60);
        Left left1 = new Left(treeId, 60, 41);
        Right right1 = new Right(treeId, 60, 74);
        Left left2 = new Left(treeId, 41, 16);
        Right right2 = new Right(treeId, 41, 53);
        Left left3 = new Left(treeId, 74, 65);
        Right right3 = new Right(treeId, 16, 25);
        Left left4 = new Left(treeId, 53, 46);
        Right right4 = new Right(treeId, 53, 55);
        Left left5 = new Left(treeId, 65, 63);
        Right right5 = new Right(treeId, 65, 70);
        Left left6 = new Left(treeId, 46, 42);
        Left left7 = new Left(treeId, 63, 62);
        Right right6 = new Right(treeId, 63, 64);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        treeService.saveLeft(left1);
        treeService.saveRight(right1);
        treeService.saveLeft(left2);
        treeService.saveRight(right2);
        treeService.saveLeft(left3);
        treeService.saveRight(right3);
        treeService.saveLeft(left4);
        treeService.saveRight(right4);
        treeService.saveLeft(left5);
        treeService.saveRight(right5);
        treeService.saveLeft(left6);
        treeService.saveLeft(left7);
        treeService.saveRight(right6);

        Assert.assertEquals(53, (int) treeService.findAncestor(treeId, 42, 55).get());
        Assert.assertEquals(53, (int) treeService.findAncestor(treeId, 46, 55).get());
        Assert.assertEquals(41, (int) treeService.findAncestor(treeId, 25, 46).get());
        Assert.assertEquals(65, (int) treeService.findAncestor(treeId, 62, 70).get());
        Assert.assertEquals(65, (int) treeService.findAncestor(treeId, 70, 64).get());
        Assert.assertEquals(60, (int) treeService.findAncestor(treeId, 42, 62).get());
        Assert.assertEquals(60, (int) treeService.findAncestor(treeId, 25, 64).get());
        Assert.assertEquals(60, (int) treeService.findAncestor(treeId, 16, 65).get());
    }

    @Test
    public void whenTreeThenAncestorsCanBeFoundMasivian() throws Exception {
        String treeId = "ad";
        Tree tree = new Tree(treeId);
        Root root = new Root(treeId, 67);
        Left left1 = new Left(treeId, 67, 39);
        Right right1 = new Right(treeId, 67, 76);
        Left left2 = new Left(treeId, 39, 28);
        Right right2 = new Right(treeId, 39, 44);
        Left left3 = new Left(treeId, 76, 74);
        Right right3 = new Right(treeId, 76, 85);
        Left left4 = new Left(treeId, 28, 29);
        Left left5 = new Left(treeId, 85, 83);
        Right right4 = new Right(treeId, 85, 87);

        treeService.saveTree(tree);
        treeService.saveRoot(root);
        treeService.saveLeft(left1);
        treeService.saveRight(right1);
        treeService.saveLeft(left2);
        treeService.saveRight(right2);
        treeService.saveLeft(left3);
        treeService.saveRight(right3);
        treeService.saveLeft(left4);
        treeService.saveLeft(left5);
        treeService.saveRight(right4);

        Assert.assertEquals(39, (int) treeService.findAncestor(treeId, 29, 44).get());
        Assert.assertEquals(67, (int) treeService.findAncestor(treeId, 44, 85).get());
        Assert.assertEquals(85, (int) treeService.findAncestor(treeId, 83, 87).get());

    }

    @Test
    public void whenNodesFindParent() {
        String tree = "tree";
        List<Node> nodes = Arrays.asList(new Node(tree, 0, 9, false, false), new Node(tree, 9, 6, true, false), new Node(tree, 6, 7, true, false), new Node(tree, 7, 4, true, false));

        Assert.assertEquals(0, (int) treeService.findParent(9, nodes));
        Assert.assertEquals(9, (int) treeService.findParent(6, nodes));
        Assert.assertEquals(6, (int) treeService.findParent(7, nodes));
        Assert.assertEquals(7, (int) treeService.findParent(4, nodes));
    }

    @Test
    public void whenNodesFindParents(){
        String tree = "tree";
        List<Node> nodes = Arrays.asList(new Node(tree, 0, 8, false, false), new Node(tree, 8, 3, true, false), new Node(tree, 3, 1, true, false),
                new Node(tree, 3, 6, false, true), new Node(tree, 6, 4, true, false), new Node(tree, 6, 7, false, true), new Node(tree, 8, 10, false, true),
                new Node(tree, 10, 14, false, true), new Node(tree, 14, 13, true, false));

        List<Integer> parents1 = treeService.findParents(4, nodes);
        List<Integer> parents2 = treeService.findParents(7, nodes);
        List<Integer> parents3 = treeService.findParents(13, nodes);
        List<Integer> parents4 = treeService.findParents(1, nodes);
        List<Integer> parents5 = treeService.findParents(6, nodes);
        List<Integer> parents6 = treeService.findParents(14, nodes);
        List<Integer> parents7 = treeService.findParents(3, nodes);
        List<Integer> parents8 = treeService.findParents(10, nodes);

        Assert.assertEquals(Arrays.asList(4,6,3,8), parents1);
        Assert.assertEquals(Arrays.asList(7,6,3,8), parents2);
        Assert.assertEquals(Arrays.asList(13,14,10,8), parents3);
        Assert.assertEquals(Arrays.asList(1,3,8), parents4);
        Assert.assertEquals(Arrays.asList(6,3,8), parents5);
        Assert.assertEquals(Arrays.asList(14,10,8), parents6);
        Assert.assertEquals(Arrays.asList(3,8), parents7);
        Assert.assertEquals(Arrays.asList(10,8), parents8);
    }

    @Test
    public void whenArraysFindFirstMatch(){
        Optional<Integer> result1 = treeService.findFirstMatch(Arrays.asList(4,6,3,8), Arrays.asList(7,6,3,8));
        Optional<Integer> result2 = treeService.findFirstMatch(Arrays.asList(4,6,3,8), Arrays.asList(1,3,8));
        Optional<Integer> result3 = treeService.findFirstMatch(Arrays.asList(4,6,3,8), Arrays.asList(13,14,10,8));
        Optional<Integer> result4 = treeService.findFirstMatch(Arrays.asList(4,6,3,8), Arrays.asList(13,14,10,9));

        Assert.assertTrue(result1.isPresent());
        Assert.assertEquals(6, (int) result1.get());
        Assert.assertTrue(result2.isPresent());
        Assert.assertEquals(3, (int) result2.get());
        Assert.assertTrue(result3.isPresent());
        Assert.assertEquals(8, (int) result3.get());
        Assert.assertFalse(result4.isPresent());
    }
}
