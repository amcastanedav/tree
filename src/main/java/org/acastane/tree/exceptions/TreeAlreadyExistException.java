package org.acastane.tree.exceptions;

/**
 * Created by angelica on 3/02/19.
 */
public class TreeAlreadyExistException extends Exception {
    public TreeAlreadyExistException(String message) {
        super(message);
    }
}
