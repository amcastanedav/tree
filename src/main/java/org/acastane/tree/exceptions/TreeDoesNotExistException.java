package org.acastane.tree.exceptions;

/**
 * Created by angelica on 3/02/19.
 */
public class TreeDoesNotExistException extends Exception {
    public TreeDoesNotExistException(String message) {
        super(message);
    }
}
