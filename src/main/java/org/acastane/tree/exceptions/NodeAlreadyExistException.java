package org.acastane.tree.exceptions;

/**
 * Created by angelica on 3/02/19.
 */
public class NodeAlreadyExistException extends Exception {
    public NodeAlreadyExistException(String message) {
        super(message);
    }
}
