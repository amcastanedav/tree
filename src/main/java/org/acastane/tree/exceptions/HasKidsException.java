package org.acastane.tree.exceptions;

/**
 * Created by angelica on 9/02/19.
 */
public class HasKidsException extends Exception {
    public HasKidsException(String message) {
        super(message);
    }
}
