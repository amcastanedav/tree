package org.acastane.tree.exceptions;

/**
 * Created by angelica on 3/02/19.
 */
public class ParentDoesNotExistException extends Exception {
    public ParentDoesNotExistException(String message) {
        super(message);
    }
}
