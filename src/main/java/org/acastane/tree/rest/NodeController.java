package org.acastane.tree.rest;

import org.acastane.tree.core.Left;
import org.acastane.tree.core.Right;
import org.acastane.tree.core.Root;
import org.acastane.tree.exceptions.HasKidsException;
import org.acastane.tree.exceptions.NodeAlreadyExistException;
import org.acastane.tree.exceptions.ParentDoesNotExistException;
import org.acastane.tree.exceptions.TreeDoesNotExistException;
import org.acastane.tree.service.TreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

/**
 * Created by angelica on 2/02/19.
 */
@RestController
@RequestMapping("/node")
public class NodeController {

    @Autowired
    private TreeService treeService;

    @GetMapping("/root/{tree}")
    ResponseEntity<String> getRoot(@PathVariable String tree) {
        Optional<Root> root = treeService.findRootByTree(tree);
        if (root.isPresent()) {
            return new ResponseEntity<>(root.get().toString(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Root for tree " + tree + " wasn't not found", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/root/{tree}")
    ResponseEntity<String> createRoot(@PathVariable String tree, @RequestBody Map<String, Object> payload) {
        try {
            Integer number = Integer.valueOf(payload.get("number").toString());
            treeService.saveRoot(new Root(tree, number));
            return new ResponseEntity<>("You created root for tree " + tree, HttpStatus.OK);
        } catch (NullPointerException | NumberFormatException e) {
            return new ResponseEntity<>("Number not found", HttpStatus.BAD_REQUEST);
        } catch (NodeAlreadyExistException | TreeDoesNotExistException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping("/left/{tree}/{parent}")
    ResponseEntity<String> getLeft(@PathVariable String tree, @PathVariable String parent){
        try {
            int p = Integer.valueOf(parent);
            Optional<Left> left = treeService.findLeftByTreeAndParent(tree, p);
            if (left.isPresent()){
                return new ResponseEntity<>(left.get().toString(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Left for tree " + tree + " and parent " + p + " was not found", HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(parent + " not an integer", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/left/{tree}")
    ResponseEntity<String> createLeft(@PathVariable String tree, @RequestBody Map<String, Object> payload){
        try {
            Integer parent = Integer.valueOf(payload.get("parent").toString());
            Integer number = Integer.valueOf(payload.get("number").toString());
            treeService.saveLeft(new Left(tree, parent, number));
            return new ResponseEntity<>("You created left " + number + " for tree " + tree + " and parent " + parent, HttpStatus.OK);
        } catch (NullPointerException | NumberFormatException e) {
            return new ResponseEntity<>("Your request must contain parent and number and must be integer", HttpStatus.BAD_REQUEST);
        } catch (NodeAlreadyExistException | ParentDoesNotExistException | TreeDoesNotExistException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping("/right/{tree}/{parent}")
    ResponseEntity<String> getRight(@PathVariable String tree, @PathVariable String parent){
        try {
            int p = Integer.valueOf(parent);
            Optional<Right> right = treeService.findRightByTreeAndParent(tree, p);
            if (right.isPresent()){
                return new ResponseEntity<>(right.get().toString(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Right for tree " + tree + " and parent " + p + " was not found", HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>(parent + " not an integer", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/right/{tree}")
    ResponseEntity<String> createRight(@PathVariable String tree, @RequestBody Map<String, Object> payload){
        try {
            Integer parent = Integer.valueOf(payload.get("parent").toString());
            Integer number = Integer.valueOf(payload.get("number").toString());
            treeService.saveRight(new Right(tree, parent, number));
            return new ResponseEntity<>("You created right " + number + " for tree " + tree + " and parent " + parent, HttpStatus.OK);
        } catch (NullPointerException | NumberFormatException e) {
            return new ResponseEntity<>("Your request must contain parent and number and must be integer", HttpStatus.BAD_REQUEST);
        } catch (NodeAlreadyExistException | ParentDoesNotExistException | TreeDoesNotExistException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PostMapping("/ancestor/{tree}")
    ResponseEntity<String> findAncestor(@PathVariable String tree, @RequestBody Map<String, Object> payload){
        try {
            Integer number1 = Integer.valueOf(payload.get("number1").toString());
            Integer number2 = Integer.valueOf(payload.get("number2").toString());
            Optional<Integer> ancestor = treeService.findAncestor(tree, number1, number2);
            if (ancestor.isPresent()) {
                return new ResponseEntity<>(ancestor.get().toString(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>("No common ancestor found", HttpStatus.NOT_FOUND);
            }
        } catch (NullPointerException | NumberFormatException e) {
            return new ResponseEntity<>("Your request must contain number1 and number2 and must be integer", HttpStatus.BAD_REQUEST);
        } catch (TreeDoesNotExistException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("{tree}")
    ResponseEntity<String> deleteNode(@PathVariable String tree, @RequestBody Map<String, Object> payload){
        try {
            Integer number = Integer.valueOf(payload.get("number").toString());
            treeService.deleteNode(tree, number);
            return new ResponseEntity<>("Node " + number + " deleted", HttpStatus.OK);
        } catch (NullPointerException | NumberFormatException e) {
            return new ResponseEntity<>("Your request must contain number and must be integer", HttpStatus.BAD_REQUEST);
        } catch (TreeDoesNotExistException | HasKidsException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }
}
