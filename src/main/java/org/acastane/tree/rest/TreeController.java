package org.acastane.tree.rest;

import org.acastane.tree.core.Tree;
import org.acastane.tree.exceptions.TreeAlreadyExistException;
import org.acastane.tree.exceptions.TreeDoesNotExistException;
import org.acastane.tree.service.TreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by angelica on 2/02/19.
 */
@RestController
@RequestMapping("/tree")
public class TreeController {

    @Autowired
    private TreeService treeService;

    @GetMapping("/{id}")
    ResponseEntity<String> getTree(@PathVariable String id) {
        Optional<Tree> tree = treeService.findTreeById(id);
        if (tree.isPresent()){
            return new ResponseEntity<>(tree.get().toString(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Tree not found with id " + id, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/{id}")
    ResponseEntity<String> createTree(@PathVariable String id) {
        try {
            treeService.saveTree(new Tree(id));
            return new ResponseEntity<>("You created tree with id " + id, HttpStatus.OK);
        } catch (TreeAlreadyExistException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping("/{id}")
    ResponseEntity<String> deleteTree(@PathVariable String id) {
        try {
            treeService.deleteTree(new Tree(id));
            return new ResponseEntity<>("You deleted tree with id " + id, HttpStatus.OK);
        } catch (TreeDoesNotExistException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }
}
