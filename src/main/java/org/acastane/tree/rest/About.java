package org.acastane.tree.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by angelica on 2/02/19.
 */
@RestController
public class About {

    @RequestMapping("/")
    ResponseEntity<String> about() {
        return new ResponseEntity<>("Technical test to Masivian", HttpStatus.OK);
    }
}
