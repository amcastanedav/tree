package org.acastane.tree.service;

import org.acastane.tree.core.*;
import org.acastane.tree.exceptions.*;
import org.acastane.tree.persistence.NodeRepository;
import org.acastane.tree.persistence.TreeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by angelica on 2/02/19.
 */
@Service
public class TreeService {

    @Autowired
    private TreeRepository treeRepository;

    @Autowired
    private NodeRepository nodeRepository;

    public void saveTree(Tree tree) throws TreeAlreadyExistException {
        if (!treeRepository.existsById(tree.getId())) {
            treeRepository.save(tree);
        } else {
            throw new TreeAlreadyExistException("Tree with id " + tree.getId() + " already exist in the database");
        }
    }

    public Optional<Tree> findTreeById(String id) {
        return treeRepository.findById(id);
    }

    public void deleteTree(Tree tree) throws TreeDoesNotExistException {
        checkTree(tree.getId());
        nodeRepository.deleteTree(tree.getId());
        treeRepository.delete(tree);
    }

    public void saveRoot(Root root) throws TreeDoesNotExistException, NodeAlreadyExistException {
        checkTree(root.getTree());
        if (!nodeRepository.existRoot(root.getTree())) {
            nodeRepository.save(root.toNode());
        } else {
            throw new NodeAlreadyExistException("Root for tree " + root.getTree() + " already exist");
        }
    }

    public Optional<Root> findRootByTree(String tree){
        if (nodeRepository.existRoot(tree)){
            return Optional.of(Root.toRoot(nodeRepository.findRootByTree(tree).get()));
        } else {
            return Optional.empty();
        }
    }

    public void saveLeft(Left left) throws TreeDoesNotExistException, ParentDoesNotExistException, NodeAlreadyExistException {
        checkTree(left.getTree());
        checkParent(left.getTree(), left.getParent());
        if (!nodeRepository.existNodeByTreeAndParent(left.getTree(), left.getParent(), true, false)){
            nodeRepository.save(left.toNode());
        } else {
            throw new NodeAlreadyExistException("Left for tree " + left.getTree() + " and parent " + left.getParent() + " already exist");
        }
    }

    public Optional<Left> findLeftByTreeAndParent(String tree, Integer parent){
        if (nodeRepository.existNodeByTreeAndParent(tree, parent, true, false)) {
            return Optional.of(Left.toLeft(nodeRepository.findNodeByTreeAndParent(tree, parent, true, false).get()));
        } else {
            return Optional.empty();
        }
    }

    public void saveRight(Right right) throws TreeDoesNotExistException, ParentDoesNotExistException, NodeAlreadyExistException {
        checkTree(right.getTree());
        checkParent(right.getTree(), right.getParent());
        if (!nodeRepository.existNodeByTreeAndParent(right.getTree(), right.getParent(), false, true)){
            nodeRepository.save(right.toNode());
        } else {
            throw new NodeAlreadyExistException("Right for tree " + right.getTree() + " and parent " + right.getParent() + " already exist");
        }
    }

    public Optional<Right> findRightByTreeAndParent(String tree, Integer parent){
        if (nodeRepository.existNodeByTreeAndParent(tree, parent, false, true)) {
            return Optional.of(Right.toRight(nodeRepository.findNodeByTreeAndParent(tree, parent, false, true).get()));
        } else {
            return Optional.empty();
        }
    }

    public void deleteNode(String tree, Integer number) throws TreeDoesNotExistException, HasKidsException {
        checkTree(tree);
        checkIfKids(tree, number);
        nodeRepository.deleteNode(tree, number);
    }

    public Optional<Integer> findAncestor(String tree, Integer number1, Integer number2) throws TreeDoesNotExistException {
        checkTree(tree);
        if (!nodeRepository.existNodeByTreeAndNumber(tree, number1) || !nodeRepository.existNodeByTreeAndNumber(tree, number2)) {
            return Optional.empty();
        }
        List<Node> nodes = nodeRepository.findNodesByTree(tree);
        List<Integer> parents1 = findParents(number1, nodes);
        List<Integer> parents2 = findParents(number2, nodes);
        return findFirstMatch(parents1, parents2);
    }

    private void checkTree(String id) throws TreeDoesNotExistException {
        if (!treeRepository.existsById(id)) {
            throw new TreeDoesNotExistException("Tree with id " + id + " doesn't exist");
        }
    }

    private void checkParent(String tree, Integer number) throws ParentDoesNotExistException {
        if (!nodeRepository.existNodeByTreeAndNumber(tree, number)) {
            throw new ParentDoesNotExistException("Parent for tree " + tree + " and number " + number + " doesn't exist");
        }
    }

    private void checkIfKids(String tree, Integer number) throws HasKidsException {
        if (nodeRepository.hasKids(tree, number)){
            throw new HasKidsException("Node " + number + " in tree " + tree + " can't be deleted, it has kids");
        }
    }

    public Integer findParent(Integer number, List<Node> nodes) {
        return nodes.stream().filter(node -> node.getNumber().equals(number)).findFirst().get().getParent();
    }

    public List<Integer> findParents(Integer number, List<Node> nodes){
        List<Integer> parents = new ArrayList<>();

        int parent = number;
        while (parent != 0) {
            parents.add(parent);
            parent = findParent(parent, nodes);
        }

        return parents;
    }

    public Optional<Integer> findFirstMatch(List<Integer> parents1, List<Integer> parents2){
        List<Integer> small;
        List<Integer> big;

        if (parents1.size() < parents2.size()){
            small = parents1;
            big = parents2;
        } else {
            small = parents2;
            big = parents1;
        }

        Optional<Integer> result = Optional.empty();

        for (Integer i:big){
            for (Integer j:small){
                if (i.equals(j)){
                    return Optional.of(i);
                }
            }
        }

        return result;
    }
}
