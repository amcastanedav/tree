package org.acastane.tree.core;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by angelica on 2/02/19.
 */
@Entity
public class Tree {

    @Id
    String id;

    public Tree(){}

    public Tree(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Tree{" +
                "id='" + id + '\'' +
                '}';
    }
}
