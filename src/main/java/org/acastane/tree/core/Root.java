package org.acastane.tree.core;

/**
 * Created by angelica on 3/02/19.
 */
public class Root extends Node {
    public Root(String tree, Integer number) {
        super(tree, 0, number, false, false);
    }

    public static Root toRoot(Node node){
        return new Root(node.getTree(), node.getNumber());
    }

    public Node toNode(){
        return new Node(tree, 0, number, false, false);
    }

    @Override
    public String toString() {
        return "Root{" +
                "tree='" + tree + '\'' +
                ", parent=" + parent +
                ", number=" + number +
                '}';
    }
}
