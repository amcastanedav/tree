package org.acastane.tree.core;

/**
 * Created by angelica on 3/02/19.
 */
public class Left extends Node {
    public Left(String tree, Integer parent, Integer number) {
        super(tree, parent, number, true, false);
    }

    public static Left toLeft(Node node){
        return new Left(node.getTree(), node.getParent(), node.getNumber());
    }

    public Node toNode(){
        return new Node(tree, parent, number, true, false);
    }

    @Override
    public String toString() {
        return "Root{" +
                "tree='" + tree + '\'' +
                ", parent=" + parent +
                ", number=" + number +
                '}';
    }
}
