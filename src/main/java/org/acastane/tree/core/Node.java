package org.acastane.tree.core;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by angelica on 2/02/19.
 */
@Entity
public class Node {

    @Id
    @GeneratedValue
    Long id;
    String tree;
    Integer parent;
    Integer number;
    Boolean leftc;
    Boolean rightc;

    public Node(){}

    public Node(String tree, Integer parent, Integer number, Boolean leftc, Boolean rightc) {
        this.tree = tree;
        this.parent = parent;
        this.number = number;
        this.leftc = leftc;
        this.rightc = rightc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTree() {
        return tree;
    }

    public void setTree(String tree) {
        this.tree = tree;
    }

    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Boolean getLeftc() {
        return leftc;
    }

    public void setLeftc(Boolean leftc) {
        this.leftc = leftc;
    }

    public Boolean getRightc() {
        return rightc;
    }

    public void setRightc(Boolean rightc) {
        this.rightc = rightc;
    }

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                ", tree='" + tree + '\'' +
                ", parent=" + parent +
                ", number=" + number +
                ", leftc=" + leftc +
                ", rightc=" + rightc +
                '}';
    }
}
