package org.acastane.tree.core;

/**
 * Created by angelica on 3/02/19.
 */
public class Right extends Node {
    public Right(String tree, Integer parent, Integer number) {
        super(tree, parent, number, false, true);
    }

    public static Right toRight(Node node){
        return new Right(node.getTree(), node.getParent(), node.getNumber());
    }

    public Node toNode(){
        return new Node(tree, parent, number, false, true);
    }

    @Override
    public String toString() {
        return "Root{" +
                "tree='" + tree + '\'' +
                ", parent=" + parent +
                ", number=" + number +
                '}';
    }
}
