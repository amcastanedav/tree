package org.acastane.tree.persistence;

import org.acastane.tree.core.Tree;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by angelica on 2/02/19.
 */
public interface TreeRepository extends CrudRepository<Tree, String> {
}
