package org.acastane.tree.persistence;

import org.acastane.tree.core.Node;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by angelica on 2/02/19.
 */
public interface NodeRepository extends CrudRepository<Node, Long> {

    @Query("SELECT count(n) > 0 FROM Node n WHERE tree = :tree AND leftc = false AND rightc = false ")
    Boolean existRoot(@Param("tree") String tree);

    @Query("SELECT n FROM Node n WHERE tree = :tree AND leftc = false AND rightc = false ")
    Optional<Node> findRootByTree(@Param("tree") String tree);

    @Query("SELECT count(n) > 0 FROM Node n WHERE tree = :tree AND number = :number")
    Boolean existNodeByTreeAndNumber(@Param("tree") String tree, @Param("number") Integer number);

    @Query("SELECT count(n) > 0 FROM Node n WHERE tree = :tree AND parent = :parent AND leftc = :leftc AND rightc = :rightc")
    Boolean existNodeByTreeAndParent(@Param("tree") String tree, @Param("parent") Integer parent, @Param("leftc") Boolean leftc, @Param("rightc") Boolean rightc);

    @Query("SELECT count(n) > 0 FROM Node n WHERE tree = :tree AND parent = :number")
    Boolean hasKids(@Param("tree") String tree, @Param("number") Integer number);

    @Query("SELECT n FROM Node n WHERE tree = :tree AND parent = :parent AND leftc = :leftc AND rightc = :rightc")
    Optional<Node> findNodeByTreeAndParent(@Param("tree") String tree, @Param("parent") Integer parent, @Param("leftc") Boolean leftc, @Param("rightc") Boolean rightc);

    @Query("SELECT n FROM Node n WHERE tree = :tree")
    List<Node> findNodesByTree(@Param("tree") String tree);

    @Transactional
    @Modifying
    @Query("DELETE FROM Node WHERE tree = :tree AND number = :number")
    void deleteNode(@Param("tree") String tree, @Param("number") Integer number);

    @Transactional
    @Modifying
    @Query("DELETE FROM Node WHERE tree = :tree")
    void deleteTree(@Param("tree") String tree);
}
